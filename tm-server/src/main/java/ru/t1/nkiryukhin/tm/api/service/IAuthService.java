package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface IAuthService {

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException;


    void invalidate(SessionDTO session) throws AbstractException;

    @NotNull
    UserDTO registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException;

    @NotNull
    SessionDTO validateToken(@Nullable String token);

}