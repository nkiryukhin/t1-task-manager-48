package ru.t1.nkiryukhin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDTORepository extends IDTORepository<SessionDTO> {

    void clear(@NotNull String userId);

    @Nullable
    List<SessionDTO> findAll(@NotNull String userId);

    @Nullable
    SessionDTO findOneById(@NotNull String id);

    int getSize();

}
